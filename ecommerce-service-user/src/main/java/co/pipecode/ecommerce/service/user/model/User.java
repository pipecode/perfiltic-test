package co.pipecode.ecommerce.service.user.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "USER")
@EqualsAndHashCode(callSuper = true)
public class User extends Audit implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ENABLED")
    private boolean enabled;

    @Column(name = "UNLOCKED")
    private boolean unlocked;

    @Column(name = "TFA_ENABLED")
    private boolean twoFactorAuthEnabled;
}


