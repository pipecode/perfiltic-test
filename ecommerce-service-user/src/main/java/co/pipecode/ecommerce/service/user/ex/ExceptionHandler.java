package co.pipecode.ecommerce.service.user.ex;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String GENERAL_ERROR = "an unexpected error has occurred, please contact support";
    private static final ConcurrentHashMap<String, Integer> STATUS_CODES = new ConcurrentHashMap<>();

    public ExceptionHandler() {
        STATUS_CODES.put(ServiceException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        STATUS_CODES.put(MethodNotAllowedException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
    }

    @org.springframework.web.bind.annotation.ExceptionHandler({Exception.class})
    public final ResponseEntity<APIResponse> handleAllExceptions(Exception exception) {
        ResponseEntity<APIResponse> result;
        final String exceptionName = exception.getClass().getSimpleName();
        final String message = exception.getMessage();
        final Integer code = STATUS_CODES.get(exceptionName);
        if (code != null) {
            final APIResponse response = new APIResponse(code, exceptionName, message);
            result = new ResponseEntity<>(response, HttpStatus.valueOf(code));
        } else {
            final APIResponse response = new APIResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), exceptionName, GENERAL_ERROR);
            result = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return result;
    }
}