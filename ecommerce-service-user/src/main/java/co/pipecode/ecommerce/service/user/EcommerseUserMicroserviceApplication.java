package co.pipecode.ecommerce.service.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaRepositories
@EnableTransactionManagement
@EnableJpaAuditing
public class EcommerseUserMicroserviceApplication {


    public static void main(String[] args) {
        SpringApplication.run(EcommerseUserMicroserviceApplication.class, args);
    }


}
