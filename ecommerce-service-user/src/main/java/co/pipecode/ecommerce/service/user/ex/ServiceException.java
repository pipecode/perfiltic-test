package co.pipecode.ecommerce.service.user.ex;

public class ServiceException extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }
}
