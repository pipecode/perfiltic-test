package co.pipecode.ecommerce.service.user.service.impl;

import co.pipecode.ecommerce.service.user.ex.ServiceException;
import co.pipecode.ecommerce.service.user.ex.UserNotFoundException;
import co.pipecode.ecommerce.service.user.model.User;
import co.pipecode.ecommerce.service.user.repository.UserRepository;
import co.pipecode.ecommerce.service.user.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceJPA implements UserService {

    private final UserRepository userRepository;

    public UserServiceJPA(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findByUsername(String username) throws UserNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new UserNotFoundException(String.format("User %s not found", username));
        }
    }

    @Override
    public void saveUser(User user) throws ServiceException {
        userRepository.save(user);
    }

    @Override
    public void updateUser(User user) throws ServiceException {
        userRepository.findById(user.getId()).map(newUser -> {
            newUser.setUsername(user.getUsername());
            newUser.setPassword(user.getPassword());
            return userRepository.save(user);
        }).orElseThrow(() -> new ServiceException("User with id " + user.getId() + "not found"));
    }

    @Override
    public List<User> findAll() throws ServiceException {
        return userRepository.findAll();
    }
}
