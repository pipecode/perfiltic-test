package co.pipecode.ecommerce.service.user.ex;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String message) {
        super(message);
    }
}
