package co.pipecode.ecommerce.service.user.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import java.io.Serializable;
import java.time.LocalDateTime;

public abstract class Audit implements Serializable {

    @Column(name = "CREATED_AT", nullable = false, updatable = false)
    @CreatedDate
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm")
    protected LocalDateTime createdDate;

    @Column(name = "UPDATED_AT")
    @LastModifiedDate
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm")
    protected LocalDateTime modifiedDate;

}