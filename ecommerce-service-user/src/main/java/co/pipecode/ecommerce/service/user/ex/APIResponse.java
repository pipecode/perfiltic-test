package co.pipecode.ecommerce.service.user.ex;

import lombok.Data;

@Data
public class APIResponse {
    private int code;
    private String event;
    private String message;

    public APIResponse(int code, String event, String message) {
        this.code = code;
        this.event = event;
        this.message = message;
    }

}
