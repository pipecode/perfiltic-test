package co.pipecode.ecommerce.service.user.service;

import co.pipecode.ecommerce.service.user.ex.ServiceException;
import co.pipecode.ecommerce.service.user.ex.UserNotFoundException;
import co.pipecode.ecommerce.service.user.model.User;

import java.util.List;

public interface UserService {

    User findByUsername(String username) throws UserNotFoundException;

    void saveUser(User user) throws ServiceException;

    void updateUser(User user) throws ServiceException;

    List<User> findAll() throws ServiceException;
}
