package co.pipecode.ecommerce.service.auth.controller;

import co.pipecode.ecommerce.service.auth.dto.AuthRequestDTO;
import co.pipecode.ecommerce.service.auth.service.SecurityService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/api/security")
public class AuthController {

    private final SecurityService securityService;

    public AuthController(SecurityService securityService) {
        this.securityService = securityService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody AuthRequestDTO authRequestDTO, HttpServletRequest request) {
        return ok().body(securityService.login(authRequestDTO, request));
    }
}