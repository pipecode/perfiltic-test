package co.pipecode.ecommerce.service.auth.ex;

public class UnauthorisedException extends RuntimeException {

    public UnauthorisedException(String message) {
        super(message);
    }
}
