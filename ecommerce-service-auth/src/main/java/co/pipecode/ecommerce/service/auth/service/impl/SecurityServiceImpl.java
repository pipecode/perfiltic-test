package co.pipecode.ecommerce.service.auth.service.impl;

import co.pipecode.ecommerce.service.auth.dto.AuthRequestDTO;
import co.pipecode.ecommerce.service.auth.dto.AuthResponseDTO;
import co.pipecode.ecommerce.service.auth.dto.UserDTO;
import co.pipecode.ecommerce.service.auth.ex.UnauthorisedException;
import co.pipecode.ecommerce.service.auth.infrastructure.UserInterface;
import co.pipecode.ecommerce.service.auth.security.JwtTokenProvider;
import co.pipecode.ecommerce.service.auth.service.SecurityService;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@Service
public class SecurityServiceImpl implements SecurityService {

    private final JwtTokenProvider jwtTokenProvider;
    private final UserInterface userInterface;

    public SecurityServiceImpl(JwtTokenProvider jwtTokenProvider, UserInterface userInterface) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.userInterface = userInterface;
    }

    @Override
    public AuthResponseDTO login(AuthRequestDTO requestDTO, HttpServletRequest request) {
        Optional<UserDTO> user = userInterface.findByUsername(requestDTO.getUsername());
        validateUserPassword.accept(requestDTO, user.get());
        validateUsername.accept(user.get());
        validateUserStatus.accept(user.get());

        AuthResponseDTO response = new AuthResponseDTO();
        response.setUsername(user.get().getUsername());
        response.setAuthToken(jwtTokenProvider.createToken(requestDTO.getUsername(), request));

        return response;
    }

    private Consumer<UserDTO> validateUsername = (user) -> {
        if (Objects.isNull(user))
            throw new UnauthorisedException("Invalid Username");
    };

    private Consumer<UserDTO> validateUserStatus = (user) -> {
        if (!user.isEnabled() || !user.isUnlocked()) {
            throw new UnauthorisedException("User is blocked or disabled");
        }
    };

    private BiConsumer<AuthRequestDTO, UserDTO> validateUserPassword = (requestDTO, user) -> {
        if (!BCrypt.checkpw(requestDTO.getPassword(), user.getPassword())) {
            throw new UnauthorisedException("Incorrect Password");
        }
    };
}
