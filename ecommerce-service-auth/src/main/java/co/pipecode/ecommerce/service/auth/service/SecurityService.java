package co.pipecode.ecommerce.service.auth.service;

import co.pipecode.ecommerce.service.auth.dto.AuthRequestDTO;
import co.pipecode.ecommerce.service.auth.dto.AuthResponseDTO;

import javax.servlet.http.HttpServletRequest;

public interface SecurityService {

    AuthResponseDTO login(AuthRequestDTO requestDTO, HttpServletRequest request);
}
