package co.pipecode.ecommerce.service.auth.infrastructure;

import co.pipecode.ecommerce.service.auth.constants.MSConstants;
import co.pipecode.ecommerce.service.auth.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

import static co.pipecode.ecommerce.service.auth.constants.MSConstants.MSUser;

@FeignClient(name = MSUser.BASE)
@Service
@RequestMapping(value = MSConstants.BASE_API)
public interface UserInterface {

    @PostMapping("/{username}")
    Optional<UserDTO> findByUsername(@PathVariable("username") String username);

}

