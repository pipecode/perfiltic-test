package co.pipecode.ecommerce.service.auth.constants;

public class MSConstants {

    public static final String BASE_API = "/api/user";

    public interface MSUser {
        String BASE = "user-service";
    }
}
