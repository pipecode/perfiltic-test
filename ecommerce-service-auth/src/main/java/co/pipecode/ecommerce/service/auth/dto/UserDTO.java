package co.pipecode.ecommerce.service.auth.dto;

import lombok.Data;

@Data
public class UserDTO {

    private long id;
    private String username;
    private String password;
    private boolean enabled;
    private boolean unlocked;
    private boolean twoFactorAuthEnabled;

}
