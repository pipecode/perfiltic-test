package co.pipecode.ecommerce.service.auth.dto;

import lombok.Data;

@Data
public class AuthResponseDTO {

    private String username;
    private String authToken;

}
