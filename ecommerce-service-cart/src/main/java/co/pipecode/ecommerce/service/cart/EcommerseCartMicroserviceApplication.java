package co.pipecode.ecommerce.service.cart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaRepositories
@EnableTransactionManagement
@EnableJpaAuditing
public class EcommerseCartMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcommerseCartMicroserviceApplication.class, args);
    }

}
