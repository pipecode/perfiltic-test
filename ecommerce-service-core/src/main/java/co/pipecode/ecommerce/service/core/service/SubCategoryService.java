package co.pipecode.ecommerce.service.core.service;

import co.pipecode.ecommerce.service.core.model.Category;
import co.pipecode.ecommerce.service.core.model.SubCategory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface SubCategoryService {

    List<SubCategory> getAll();

    Optional<SubCategory> getById(long id);

    SubCategory create(SubCategory subCategory);

    SubCategory update(SubCategory subCategory);

    void deleteById(long id);
}
