package co.pipecode.ecommerce.service.core.service.impl;

import co.pipecode.ecommerce.service.core.model.Product;
import co.pipecode.ecommerce.service.core.repository.ProductRepository;
import co.pipecode.ecommerce.service.core.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> getAllByCategory(long subCategoryId) {
        return productRepository.findAllByCategory(subCategoryId);
    }

    @Override
    public List<Product> getAllBySubCategory(long categoryId) {
        return productRepository.findAllBySubCategoryId(categoryId);
    }

    @Override
    public Optional<Product> getById(long id) {
        return productRepository.findById(id);
    }

    @Override
    public Product create(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product update(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void deleteById(long id) {
        productRepository.deleteById(id);
    }
}
