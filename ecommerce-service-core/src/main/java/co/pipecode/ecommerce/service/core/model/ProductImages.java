package co.pipecode.ecommerce.service.core.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@Table(name = "PRODUCT_IMAGES")
public class ProductImages extends Audit {

    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    @JsonBackReference
    private Product product;

    @Column(name = "IMAGE_URL", columnDefinition="TEXT")
    private String imageUrl;

    public ProductImages() {

    }
}
