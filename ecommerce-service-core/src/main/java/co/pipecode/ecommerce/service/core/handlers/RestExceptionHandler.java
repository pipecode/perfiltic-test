package co.pipecode.ecommerce.service.core.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String GENERAL_ERROR = "Ha ocurrido un error en el servidor, por favor contacte al administrador";
    private static final ConcurrentHashMap<String, Integer> STATUS_CODES = new ConcurrentHashMap<>();

    public RestExceptionHandler() {
        // STATUS_CODES.put(ServiceException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        STATUS_CODES.put(MethodNotAllowedException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler({Exception.class})
    public final ResponseEntity<ApiResponse> handleAllExceptions(Exception exception) {
        ResponseEntity<ApiResponse> result;
        final String exceptionName = exception.getClass().getSimpleName();
        final String message = exception.getMessage();
        final Integer code = STATUS_CODES.get(exceptionName);
        if (code != null) {
            final ApiResponse response = new ApiResponse(code, exceptionName, message);
            result = new ResponseEntity<>(response, HttpStatus.valueOf(code));
        } else {
            final ApiResponse response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), exceptionName, GENERAL_ERROR);
            result = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return result;
    }
}
