package co.pipecode.ecommerce.service.core.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Audit {

    @Column(name = "CREATED_AT", nullable = false, updatable = false)
    @CreatedDate
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm")
    protected LocalDateTime createdDate;

    @Column(name = "UPDATED_AT")
    @LastModifiedDate
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm")
    protected LocalDateTime modifiedDate;
}
