package co.pipecode.ecommerce.service.core.repository;

import co.pipecode.ecommerce.service.core.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
