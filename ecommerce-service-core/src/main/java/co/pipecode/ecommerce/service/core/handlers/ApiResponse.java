package co.pipecode.ecommerce.service.core.handlers;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiResponse {

    private int code;
    private String event;
    private String message;

    public ApiResponse() {

    }
}
