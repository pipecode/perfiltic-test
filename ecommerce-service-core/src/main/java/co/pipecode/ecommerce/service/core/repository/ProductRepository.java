package co.pipecode.ecommerce.service.core.repository;

import co.pipecode.ecommerce.service.core.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "SELECT * FROM PRODUCT WHERE subcategory_id IN (SELECT ID FROM SUB_CATEGORY WHERE category_id = :categoryId  )", nativeQuery = true)
    List<Product> findAllByCategory(@Param("categoryId") long categoryId);

    List<Product> findAllBySubCategoryId(long subCategoryId);
}
