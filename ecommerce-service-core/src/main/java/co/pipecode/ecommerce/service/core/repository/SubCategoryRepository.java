package co.pipecode.ecommerce.service.core.repository;

import co.pipecode.ecommerce.service.core.model.SubCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubCategoryRepository extends JpaRepository<SubCategory, Long> {
}
