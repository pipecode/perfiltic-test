package co.pipecode.ecommerce.service.core.rest;

import co.pipecode.ecommerce.service.core.model.Category;
import co.pipecode.ecommerce.service.core.model.Product;
import co.pipecode.ecommerce.service.core.model.SubCategory;
import co.pipecode.ecommerce.service.core.service.ProductService;
import co.pipecode.ecommerce.service.core.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductRestController {

    @Autowired
    private ProductService productService;

    @Autowired
    private SubCategoryService subCategoryService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Product>> getAllProducts() throws Exception {
        return new ResponseEntity<>(productService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/category/{categoryId}")
    @ResponseBody
    public ResponseEntity<List<Product>> getAllProductsByCategory(@PathVariable(name = "categoryId") long categoryId) throws Exception {
        return new ResponseEntity<>(productService.getAllByCategory(categoryId), HttpStatus.OK);
    }

    @GetMapping("/subcategory/{subCategoryId}")
    @ResponseBody
    public ResponseEntity<List<Product>> getAllProductsBySubCategory(@PathVariable(name = "subCategoryId") long subCategoryId) throws Exception {
        return new ResponseEntity<>(productService.getAllBySubCategory(subCategoryId), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Product getProductById(@PathVariable(name = "id") long id) throws Exception {
        return productService.getById(id).orElseThrow(() -> new Exception("Category with id " + id + "not found"));
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable(name = "id") long id) {
        productService.deleteById(id);
    }

    @PostMapping("/subcategory/{id}")
    public Product createProduct(@PathVariable(name = "id") long id, @RequestBody Product product) throws Exception {
        SubCategory category = subCategoryService.getById(id).orElseThrow(() -> new Exception("SunbCategory with id " + id + " not found"));
        product.setSubCategory(category);
        return productService.create(product);
    }
}
