package co.pipecode.ecommerce.service.core.service.impl;

import co.pipecode.ecommerce.service.core.model.SubCategory;
import co.pipecode.ecommerce.service.core.repository.SubCategoryRepository;
import co.pipecode.ecommerce.service.core.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubCategoryServiceImpl implements SubCategoryService {

    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Override
    public List<SubCategory> getAll() {
        return subCategoryRepository.findAll();
    }

    @Override
    public Optional<SubCategory> getById(long id) {
        return subCategoryRepository.findById(id);
    }

    @Override
    public SubCategory create(SubCategory subCategory) {
        return subCategoryRepository.save(subCategory);
    }

    @Override
    public SubCategory update(SubCategory subCategory) {
        return subCategoryRepository.save(subCategory);
    }

    @Override
    public void deleteById(long id) {
        subCategoryRepository.deleteById(id);
    }
}
