package co.pipecode.ecommerce.service.core.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@Table(name = "PRODUCT")
public class Product extends Audit {

    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "POUND_WEIGHT")
    private double poundWeight;

    @Column(name = "USD_PRICE")
    private double usdPrice;

    @ManyToOne
    @JoinColumn(name = "subcategory_id")
    private SubCategory subCategory;

    @OneToMany(mappedBy = "product")
    @JsonManagedReference
    private List<ProductImages> images;

    public Product() {

    }
}
