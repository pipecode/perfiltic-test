package co.pipecode.ecommerce.service.core.service;

import co.pipecode.ecommerce.service.core.model.Category;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    List<Category> getAll();

    Optional<Category> getById(long id);

    Category create(Category category);

    Category update(Category category);

    void deleteById(long id);
}
