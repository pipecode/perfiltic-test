package co.pipecode.ecommerce.service.core;

import co.pipecode.ecommerce.service.core.model.Category;
import co.pipecode.ecommerce.service.core.model.Product;
import co.pipecode.ecommerce.service.core.model.ProductImages;
import co.pipecode.ecommerce.service.core.model.SubCategory;
import co.pipecode.ecommerce.service.core.repository.ProductImageRepository;
import co.pipecode.ecommerce.service.core.service.CategoryService;
import co.pipecode.ecommerce.service.core.service.ProductService;
import co.pipecode.ecommerce.service.core.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaRepositories
@EnableTransactionManagement
@EnableJpaAuditing
public class EcommerseCoreMicroserviceApplication implements CommandLineRunner {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductImageRepository productImageRepository;

    public static void main(String[] args) {
        SpringApplication.run(EcommerseCoreMicroserviceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Category categoryTecnologia = new Category();
        categoryTecnologia.setName("Tecnologia");

        Category categoryDeportes = new Category();
        categoryDeportes.setName("Deportes");

        categoryService.create(categoryTecnologia);
        categoryService.create(categoryDeportes);

        SubCategory subCategorySmartphones = new SubCategory();
        subCategorySmartphones.setName("SmartPhones");
        subCategorySmartphones.setImageUrl("https://cdn2.hubspot.net/hubfs/452033/Walmart-app-banner-blog.png");
        subCategorySmartphones.setCategory(categoryTecnologia);

        SubCategory subCategoryComputadores = new SubCategory();
        subCategoryComputadores.setName("Computadores");
        subCategoryComputadores.setImageUrl("https://www.fiercepc.co.uk/media/wysiwyg/CUSTOM-PC-BANNER.jpg");
        subCategoryComputadores.setCategory(categoryTecnologia);

        SubCategory subCategoryCiclismo = new SubCategory();
        subCategoryCiclismo.setName("Ciclismo");
        subCategoryCiclismo.setImageUrl("https://www.khebikes.com/media/image/c9/6c/f3/KHE-Banner-Bikes-BMX-bicycle.jpg");
        subCategoryCiclismo.setCategory(categoryDeportes);

        subCategoryService.create(subCategorySmartphones);
        subCategoryService.create(subCategoryComputadores);
        subCategoryService.create(subCategoryCiclismo);


        Product productGalaxy = new Product();
        productGalaxy.setName("Samsung Galaxy Note 10+");
        productGalaxy.setUsdPrice(400);
        productGalaxy.setPoundWeight(0.6);
        productGalaxy.setSubCategory(subCategorySmartphones);

        ProductImages productImagesSamsung1 = new ProductImages();
        productImagesSamsung1.setProduct(productGalaxy);
        productImagesSamsung1.setImageUrl("https://cnet2.cbsistatic.com/img/wAgYT4Ze6KX6xskroWf8ZJB4W28=/940x0/2019/08/27/fd0f5eb9-0637-4a39-a227-a10046d6e99e/galaxy-note-10-plus.jpg");

        ProductImages productImagesSamsung2 = new ProductImages();
        productImagesSamsung2.setProduct(productGalaxy);
        productImagesSamsung2.setImageUrl("https://cnet2.cbsistatic.com/img/GR0hJOpTGH0f3HWKUoPFtBFm-tI=/470x353/2019/08/16/bd9da081-8222-4c7e-91de-14e59aff4e4b/galaxy-note-10-plus-4.jpg");

        ProductImages productImagesSamsung3 = new ProductImages();
        productImagesSamsung3.setProduct(productGalaxy);
        productImagesSamsung3.setImageUrl("https://cnet2.cbsistatic.com/img/wAgYT4Ze6KX6xskroWf8ZJB4W28=/940x0/2019/08/27/fd0f5eb9-0637-4a39-a227-a10046d6e99e/galaxy-note-10-plus.jpg");

        Product productMotorola = new Product();
        productMotorola.setName("Moto One");
        productMotorola.setUsdPrice(200);
        productMotorola.setPoundWeight(0.6);
        productMotorola.setSubCategory(subCategorySmartphones);

        ProductImages productImagesMoto1 = new ProductImages();
        productImagesMoto1.setProduct(productMotorola);
        productImagesMoto1.setImageUrl("https://images-eu.ssl-images-amazon.com/images/I/51JO9CSLhDL.jpg");

        ProductImages productImagesMoto2 = new ProductImages();
        productImagesMoto2.setProduct(productMotorola);
        productImagesMoto2.setImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTd-OBDRZ3mJePBUf1DFyk2WN3zhmOadyfCpWncmTPiZ6KBKfyV&usqp=CAU");

        ProductImages productImagesMoto3 = new ProductImages();
        productImagesMoto3.setProduct(productMotorola);
        productImagesMoto3.setImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSWemjOC_xNiF5O-qQp5iJ9KR08O7TccMbALomtoY-H-17j5wJK&usqp=CAU");

        Product productLaptop = new Product();
        productLaptop.setName("Azus GL 552VW");
        productLaptop.setUsdPrice(600);
        productLaptop.setPoundWeight(4);
        productLaptop.setSubCategory(subCategoryComputadores);

        ProductImages productImageslaptop1 = new ProductImages();
        productImageslaptop1.setProduct(productLaptop);
        productImageslaptop1.setImageUrl("https://images-na.ssl-images-amazon.com/images/I/51s9niAyueL._SL500_AC_SS350_.jpg");

        ProductImages productImageslaptop2 = new ProductImages();
        productImageslaptop2.setProduct(productLaptop);
        productImageslaptop2.setImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSuvsddXf801WsvufopJ_wVgTo5DutXSpOfY03byeaJs-WiJisc&usqp=CAU");

        ProductImages productImageslaptop3 = new ProductImages();
        productImageslaptop3.setProduct(productLaptop);
        productImageslaptop3.setImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSuvsddXf801WsvufopJ_wVgTo5DutXSpOfY03byeaJs-WiJisc&usqp=CAU");

        Product productCasco = new Product();
        productCasco.setName("Casco BMX Pro Neon");
        productCasco.setUsdPrice(200);
        productCasco.setPoundWeight(2);
        productCasco.setSubCategory(subCategoryCiclismo);

        ProductImages productImagesCasco1 = new ProductImages();
        productImagesCasco1.setProduct(productCasco);
        productImagesCasco1.setImageUrl("https://cdn3.materiel-velo.com/35486-mv_detail_product/casco-integral-shot-revolt-neon-yellow.jpg");

        ProductImages productImagesCasco2 = new ProductImages();
        productImagesCasco2.setProduct(productCasco);
        productImagesCasco2.setImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRz1Ncu_zj4fFvP6d5H0VFcIDdL3LjwmSBgLLiuKzirToq-7MY4&usqp=CAU");


        ProductImages productImagesCasco3 = new ProductImages();
        productImagesCasco3.setProduct(productCasco);
        productImagesCasco3.setImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRqehcXAG0PGd05RYVTbPADTAWec1Qhp7tq-O1-SZJn0iP0c5_6&usqp=CAU");


        productService.create(productGalaxy);
        productService.create(productMotorola);
        productService.create(productLaptop);
        productService.create(productCasco);

        productImageRepository.save(productImagesSamsung1);
        productImageRepository.save(productImagesSamsung2);
        productImageRepository.save(productImagesSamsung3);

        productImageRepository.save(productImagesMoto1);
        productImageRepository.save(productImagesMoto2);
        productImageRepository.save(productImagesMoto3);

        productImageRepository.save(productImageslaptop1);
        productImageRepository.save(productImageslaptop2);
        productImageRepository.save(productImageslaptop3);

        productImageRepository.save(productImagesCasco1);
        productImageRepository.save(productImagesCasco2);
        productImageRepository.save(productImagesCasco3);

    }
}
