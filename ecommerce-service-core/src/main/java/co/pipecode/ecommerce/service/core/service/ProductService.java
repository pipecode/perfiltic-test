package co.pipecode.ecommerce.service.core.service;

import co.pipecode.ecommerce.service.core.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    List<Product> getAll();

    List<Product> getAllByCategory(long subCategoryId);

    List<Product> getAllBySubCategory(long categoryId);

    Optional<Product> getById(long id);

    Product create(Product product);

    Product update(Product product);

    void deleteById(long id);
}
