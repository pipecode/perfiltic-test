package co.pipecode.ecommerce.service.core.rest;

import co.pipecode.ecommerce.service.core.model.Category;
import co.pipecode.ecommerce.service.core.model.SubCategory;
import co.pipecode.ecommerce.service.core.service.CategoryService;
import co.pipecode.ecommerce.service.core.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/category")
public class CategoryRestController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubCategoryService subCategoryService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Category>> getAllCategories() throws Exception {
        return new ResponseEntity<>(categoryService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Category getCategoryById(@PathVariable(name = "id") long id) throws Exception {
        return categoryService.getById(id).orElseThrow(() -> new Exception("Category with id " + id + "not found"));
    }

    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable(name = "id") long id) {
        categoryService.deleteById(id);
    }

    @PostMapping
    public Category createCategory(@RequestBody Category category) {
        return categoryService.create(category);
    }

    @PostMapping("/{id}/subcategory")
    public SubCategory createSubCategory(@PathVariable(name = "id") long id, @RequestBody SubCategory subCategory) throws Exception {
        Category category = categoryService.getById(id).orElseThrow(() -> new Exception("Category with id " + id + " not found"));
        subCategory.setCategory(category);
        return subCategoryService.create(subCategory);
    }

    @PutMapping("/{id}")
    private Category updateCategory(@PathVariable(name = "id") long id, @RequestBody Category category) throws Exception {
        return categoryService.getById(id).map(newObj -> {
            newObj.setName(category.getName());
            return categoryService.update(newObj);
        }).orElseThrow(() -> new Exception("Category with id " + id + "not found"));
    }

    @PutMapping("/subcategory/{id}}")
    private SubCategory updateSubCategory(@PathVariable(name = "id") long id, @RequestBody SubCategory category) throws Exception {
        return subCategoryService.getById(id).map(newObj -> {
            newObj.setName(category.getName());
            newObj.setImageUrl(category.getImageUrl());
            return subCategoryService.update(newObj);
        }).orElseThrow(() -> new Exception("SubCategory with id " + id + "not found"));
    }

    @DeleteMapping("/subcategory/{id}")
    public void deleteSubCategory(@PathVariable(name = "id") long id) {
        subCategoryService.deleteById(id);
    }

    @GetMapping("/subcategory/{id}")
    @ResponseBody
    public SubCategory getSubCategoryById(@PathVariable(name = "id") long id) throws Exception {
        return subCategoryService.getById(id).orElseThrow(() -> new Exception("SubCategory with id " + id + "not found"));
    }
}
