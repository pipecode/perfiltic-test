package co.pipecode.ecommerce.service.core.repository;

import co.pipecode.ecommerce.service.core.model.ProductImages;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductImageRepository extends JpaRepository<ProductImages, Long> {
}
