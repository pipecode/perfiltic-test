import { Component, OnInit } from '@angular/core';

interface Currency {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {

  currencies: Currency[] = [
    {value: 1, viewValue: 'USD'},
    {value: 2, viewValue: 'COP'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
