import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopComponent } from './shop.component';
import { ProductsComponent } from './products/products.component';

const routes: Routes = [
  {
    path: 'shop',
    component: ShopComponent,
    children: [
      {
        path: 'products',
        component: ProductsComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopRoutingModule {}
