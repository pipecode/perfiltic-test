import { Component, OnInit } from '@angular/core';
import { ProductService } from '../shared/product.service';
import { Product } from '../shared/model/product-model';
import { Category } from '../shared/model/category-model';
import { CategoryService } from '../shared/category.service';
import { SubCategory } from '../shared/model/sub-category-model';

export interface NavItem {
  displayName: string;
  disabled?: boolean;
  iconName: string;
  route?: string;
  children?: NavItem[];
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  imageUrl = 'https://adventistm2.web2market.com/pub/media/descriptions/free-shipping-banner.jpg';
  panelOpenState = true;

  products: Array<Product> = [];
  categories: Array<Category> = [];
  mySlideOptions = { items: 1, dots: true, nav: false };
  myCarouselOptions = { items: 3, dots: true, nav: true };

  constructor(private productService: ProductService, private categoryService: CategoryService) {
    this.loadAllCategories();
  }

  ngOnInit() {}

  loadAllProducts() {
    this.productService.getAllProducts().subscribe((data) => {
      this.products = data;
    });
  }

  loadAllCategories() {
    this.categoryService.getAllCategories().subscribe((data) => {
      this.categories = data;
      this.loadAllProducts();
    });
  }

  filter(subCategoryId: number, subCategoryImage: string) {
    if (subCategoryId === 0) {
      this.loadAllProducts();
    } else {
      this.productService.getAllProductsBySubcategory(subCategoryId).subscribe((data) => {
        this.products = data;
        this.imageUrl = subCategoryImage;
      });
    }
  }
}
