import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './model/product-model';
import { retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private httpClient: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  getAllProductsBySubcategory(subcategoryId: number): Observable<any> {
    return this.httpClient.get<Product>('/api/product/subcategory/' + subcategoryId).pipe(retry(1));
  }

  getAllProducts(): Observable<any> {
    return this.httpClient.get<Product>('/api/product').pipe(retry(1));
  }
}
