import { SubCategory } from './sub-category-model';
import { Image } from './image-model';

export class Product {
  id: number;
  name: string;
  poundWeight: number;
  usdPrice: number;
  subCategory: SubCategory;
  images: Array<Image>;
}
