export class SubCategory {
  id: number;
  name: string;
  imageUrl: string;
}
