import { SubCategory } from './sub-category-model';

export class Category {
  id: number;
  name: string;
  subCategories: Array<SubCategory>;
}
