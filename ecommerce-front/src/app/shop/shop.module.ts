import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShopRoutingModule } from './shop-routing.module';
import { ProductsComponent } from './products/products.component';
import { ShopComponent } from './shop.component';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '../layout/layout.module';
import { MenuComponent } from './menu/menu.component';


@NgModule({
  declarations: [ProductsComponent, ShopComponent, MenuComponent],
  imports: [
    CommonModule,
    ShopRoutingModule,
    RouterModule,
    LayoutModule
  ]
})
export class ShopModule { }
