package co.pipecode.ecommerce.authserver.infrastructure;

import co.pipecode.ecommerce.authserver.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

import static co.pipecode.ecommerce.authserver.constants.MSConstants.BASE_API;
import static co.pipecode.ecommerce.authserver.constants.MSConstants.USER_MICROSERVICE;

@FeignClient(name = USER_MICROSERVICE)
@Service
@RequestMapping(value = BASE_API)
public interface UserInterface {

    @PostMapping("/{username}")
    Optional<UserDTO> findByUsername(@PathVariable("username") String username);

}
