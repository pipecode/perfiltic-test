package co.pipecode.ecommerce.authserver.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class UserDTO implements Serializable {

    private Long id;

    private String username;

    private String emailAddress;

    private String password;

    private Character status;

    private Integer loginAttempt;

    private Date createdDate;

    private Date lastModifiedDate;

    private List<String> roles = new ArrayList<>();
}