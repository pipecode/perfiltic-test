package co.pipecode.ecommerce.authserver.constants;

public class MSConstants {

    public static final String LOGIN_MICROSERVICE = "/auth-service/api/security/*";
    public static final String CORE_MICROSERVICE_CATEGORY = "/core-service/api/category/*";
    public static final String CORE_MICROSERVICE_PRODUCT = "/core-service/api/product/*";

    public static final String CORE_MICROSERVICE_CATEGORY_GATEWAY = "/api/category/ *";
    public static final String CORE_MICROSERVICE_PRODUCT_GATEWAY = "/api/product/*";

    public static final String USER_MICROSERVICE = "user-service";
    public static final String BASE_API = "/api/user";

    public interface MSUser {
        String FETCH_USER_BY_USERNAME = "/fetch-admin/{username}";
    }
}
