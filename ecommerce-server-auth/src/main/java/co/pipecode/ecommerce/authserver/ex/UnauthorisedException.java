package co.pipecode.ecommerce.authserver.ex;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class UnauthorisedException extends RuntimeException {
    private ErrorResponse errorResponse;

    public UnauthorisedException(String message, String techMessage) {
        super(message);

        errorResponse = new ErrorResponse();

        errorResponse.setTechMessage(techMessage);
        errorResponse.setMessage(message);
        errorResponse.setStatus(HttpStatus.UNAUTHORIZED);
    }
}