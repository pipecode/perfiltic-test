package co.pipecode.ecommerce.authserver.constants;

public class ErrorConstants {

    public interface TokenInvalid {
        String TECH_MESSAGE = "Request not authorized.";
        String MESSAGE = "Unmatched JWT token.";
    }
}
